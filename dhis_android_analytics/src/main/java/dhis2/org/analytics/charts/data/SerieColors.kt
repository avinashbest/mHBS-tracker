package dhis2.org.analytics.charts.data

import android.graphics.Color

class SerieColors {
    companion object {
        fun getColors(): List<Int> = listOf(
            Color.parseColor("#E71409"),
            Color.parseColor("#49B044"),
            Color.parseColor("#994BA5"),
            Color.parseColor("#337DBA"),
            Color.parseColor("#FF7F00"),
            Color.parseColor("#999999"),
            Color.parseColor("#F97FC0"),
            Color.parseColor("#2C2C2C"),
            Color.parseColor("#A85621")
        )
    }
}
