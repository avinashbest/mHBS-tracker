package dhis2.org.analytics.charts.providers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\b"}, d2 = {"Ldhis2/org/analytics/charts/providers/RuleEngineNutritionDataProviderImpl;", "Ldhis2/org/analytics/charts/providers/NutritionDataProvider;", "()V", "getNutritionData", "", "Ldhis2/org/analytics/charts/data/SerieData;", "nutritionChartType", "Ldhis2/org/analytics/charts/data/NutritionChartType;", "dhis_android_analytics_dhisDebug"})
public final class RuleEngineNutritionDataProviderImpl implements dhis2.org.analytics.charts.providers.NutritionDataProvider {
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.util.List<dhis2.org.analytics.charts.data.SerieData> getNutritionData(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.data.NutritionChartType nutritionChartType) {
        return null;
    }
    
    public RuleEngineNutritionDataProviderImpl() {
        super();
    }
}