package dhis2.org.analytics.charts.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J&\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Ldhis2/org/analytics/charts/mappers/ProgramIndicatorToGraph;", "", "periodStepProvider", "Ldhis2/org/analytics/charts/providers/PeriodStepProvider;", "chartCoordinatesProvider", "Ldhis2/org/analytics/charts/providers/ChartCoordinatesProvider;", "(Ldhis2/org/analytics/charts/providers/PeriodStepProvider;Ldhis2/org/analytics/charts/providers/ChartCoordinatesProvider;)V", "map", "Ldhis2/org/analytics/charts/data/Graph;", "programIndicator", "Lorg/hisp/dhis/android/core/program/ProgramIndicator;", "stageUid", "", "teiUid", "stagePeriod", "Lorg/hisp/dhis/android/core/period/PeriodType;", "dhis_android_analytics_dhisDebug"})
public final class ProgramIndicatorToGraph {
    private final dhis2.org.analytics.charts.providers.PeriodStepProvider periodStepProvider = null;
    private final dhis2.org.analytics.charts.providers.ChartCoordinatesProvider chartCoordinatesProvider = null;
    
    @org.jetbrains.annotations.NotNull()
    public final dhis2.org.analytics.charts.data.Graph map(@org.jetbrains.annotations.NotNull()
    org.hisp.dhis.android.core.program.ProgramIndicator programIndicator, @org.jetbrains.annotations.NotNull()
    java.lang.String stageUid, @org.jetbrains.annotations.NotNull()
    java.lang.String teiUid, @org.jetbrains.annotations.NotNull()
    org.hisp.dhis.android.core.period.PeriodType stagePeriod) {
        return null;
    }
    
    public ProgramIndicatorToGraph(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.providers.PeriodStepProvider periodStepProvider, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.providers.ChartCoordinatesProvider chartCoordinatesProvider) {
        super();
    }
}