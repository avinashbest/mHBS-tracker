package dhis2.org.analytics.charts.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\r"}, d2 = {"Ldhis2/org/analytics/charts/mappers/GraphToLineData;", "", "()V", "coordinateToEntryMapper", "Ldhis2/org/analytics/charts/mappers/GraphCoordinatesToEntry;", "getCoordinateToEntryMapper", "()Ldhis2/org/analytics/charts/mappers/GraphCoordinatesToEntry;", "coordinateToEntryMapper$delegate", "Lkotlin/Lazy;", "map", "Lcom/github/mikephil/charting/data/LineData;", "graph", "Ldhis2/org/analytics/charts/data/Graph;", "dhis_android_analytics_dhisDebug"})
public final class GraphToLineData {
    private final kotlin.Lazy coordinateToEntryMapper$delegate = null;
    
    private final dhis2.org.analytics.charts.mappers.GraphCoordinatesToEntry getCoordinateToEntryMapper() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.github.mikephil.charting.data.LineData map(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.data.Graph graph) {
        return null;
    }
    
    public GraphToLineData() {
        super();
    }
}